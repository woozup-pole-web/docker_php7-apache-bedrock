FROM refpg1/php7-apache

COPY default-ssl.conf /etc/apache2/sites-available/default-ssl.conf
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

ENTRYPOINT ["/run.sh"]
CMD apache2-foreground

